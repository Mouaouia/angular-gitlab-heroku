import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Registreren',
      description: 'Hiermee kan een gebruiker registreren',
      scenario: [
        'Gebruiker klikt op de registeren knop in de navbar',
        'In het registreren scherm vult de gebruiker zijn gegevens in',
        'Gebruiker klikt op registreren om zijn gegevens te registreren en om in te loggen'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De gebruiker is geregistreerd'
    },
    {
      id: 'UC-02',
      name: 'Inloggen',
      description: 'Hiermee kan een bestaande gebruiker inloggen.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker is geregistreerd',
      postcondition: 'De gebruiker is ingelogd'
    },
    {
      id: 'UC-03',
      name: 'Verkijgen van alle games',
      description: 'Een gebruiker kan alle games en de informatie daarvan bekijken',
      scenario: [
        'De applicatie toont alle games',
        'Gebruiker klikt op een bepaalde game',
        'De applicatie toont alle informatie over de game'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'De games worden getoond en details daarvan worden weergeven'
    },
    {
      id: 'UC-04',
      name: 'Opzoeken van een bepaalde game',
      description: 'Een gebruiker kan zo informatie opzoeken over een game',
      scenario: [
        'Gebruiker navigeert naar de pagina games',
        'De applicatie toont een overzicht van alle games',
        'Gebruiker zoekt hier naar de naam/ title van de game of zoekt deze met behulp van de zoekbalk'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'De game is opgezocht en details daarvan worden weergeven'
    },
    {
      id: 'UC-5',
      name: 'Create (CRUD)',
      description: 'Create (CRUD) operaties',
      scenario: [
        'Gebruiker navigeert naar New Game scherm',
        'De applicatie laad een invulscherm voor het aanmaken van de entiteit',
        'Gebruiker vult de benodigde velden en klikt vervolgens op create game',
        'De admin vult deze in en klikt vervolgens op create',
        'De applicatie voert de juiste commandos door'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Entiteit informatie word aangemaakt'
    },
    {
      id: 'UC-06',
      name: 'Delete(CRUD)',
      description: 'Delete(CRUD) operaties voor administrator',
      scenario: [
        'De admin kiest de desbetreffende entiteit/game waar een actie op uitgevoegd zal worden',
        'De applicatie laat een scherm zien met de details van de applicatie',
        'De admin klikt op een delete button',
        'De applicatie zorgt dat de juiste commandos worden uitgevoerd op de database'
      ],
      actor: 'Admin/ Creater van de game',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Entiteit informatie word verwijderd'
    },
    {
      id: 'UC-07',
      name: 'Update(CRUD)',
      description: 'Update(CRUD) operaties voor administrator',
      scenario: [
        'De admin kiest de desbetreffende entiteit waar een actie op uitgevoegd zal worden',
        'De applicatie laat een scherm zien met de details van de applicatie',
        'De admin past de gegevens aan die aangepast moeten worden en klikt vervlogens op Save',
        'De applicatie zorgt dat de juiste commandos worden uitgevoerd op de database'
      ],
      actor: 'Admin/ Creater van de game',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Entiteit informatie word geupdate'
    },
    {
      id: 'UC-08',
      name: 'Toevoegen (CRUD) van developer',
      description: 'Developer toevoegen aan de game',
      scenario: [
        'De admin kiest de desbetreffende game waar een actie op uitgevoegd zal worden',
        'De applicatie laat een scherm zien met de details van de applicatie',
        'De admin klikt op Add developer',
        'De applicatie zorgt dat de juiste scherm geopend wordt',
        'De admin voegt de gegevens aan en klikt vervlogens op Save developer',
        'De applicatie zorgt dat de juiste commandos worden uitgevoerd op de database'
      ],
      actor: 'Admin/ Creater van de game',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Developer is toegevoegd aan de game'
    },
    {
      id: 'UC-09',
      name: 'Toevoegen (CRUD) van charachter',
      description: 'Charachter toevoegen aan de game',
      scenario: [
        'De admin kiest de desbetreffende game waar een actie op uitgevoegd zal worden',
        'De applicatie laat een scherm zien met de details van de applicatie',
        'De admin klikt op Add charachter',
        'De applicatie zorgt dat de juiste scherm geopend wordt',
        'De admin voegt de gegevens aan en klikt vervlogens op Save charachter',
        'De applicatie zorgt dat de juiste commandos worden uitgevoerd op de database'
      ],
      actor: 'Admin/ Creater van de game',
      precondition: 'Gebruiker is ingelogd',
      postcondition: 'Charachter is toegevoegd aan de game'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
