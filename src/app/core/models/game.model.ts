import { Developer } from './developer.model'
import { Charachter } from './charachter.model'

export class Game {
  id: string
  naam: string
  description: string
  platform: string
  category: string
  releasedate: Date
  developer: Developer
  charachter: Charachter
  user: string
}
